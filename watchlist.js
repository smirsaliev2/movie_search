const apiKey = 'cc146581'
let moviesData = new Array()
let watchlist = JSON.parse(localStorage.getItem('watchlist'))

document.addEventListener('DOMContentLoaded', () => {
    fetchMoviesData()
})

document.addEventListener('click', (e) => {
    if (e.target.dataset.imdbid) {
        removeFromWatchlist(e.target.dataset.imdbid)
    }
})


function removeFromWatchlist(imdbid) {
    watchlist = watchlist.filter(id => id !== imdbid)
    moviesData = moviesData.filter(movie => movie.imdbID !== imdbid)
    localStorage.setItem('watchlist', JSON.stringify(watchlist))
    render()
}

async function fetchMoviesData() {
    if (watchlist.length > 0) {
        for (let movieImdbID of watchlist) {
            const response = await fetch(`https://www.omdbapi.com/?apikey=${apiKey}&i=${movieImdbID}`)
            const movieData = await response.json()
            moviesData.push(movieData)
        }
        render()
        console.log(moviesData)
    }
}

function render() {
    let watchlistHtml = ''
    for (let movie of moviesData) {
        watchlistHtml += getMovieHtml(movie)
    }
    document.querySelector('main').innerHTML = watchlistHtml
}


function getMovieHtml(movieData) {
    const { Genre, imdbRating, Poster, Plot, Runtime, Title, imdbID  } = movieData
    return `
        <section class="movie">
                <img src="${Poster}" class="poster">
                <div class="about-movie">
                    <div class="movie-title">${Title}  
                        <span class="stars">⭐${imdbRating}</span>
                    </div>
                    <div class="movie-info">
                        <span>${Runtime} min</span>
                        <span>${Genre}</span>
                        <button class='watchlist-remove-btn' data-imdbID="${imdbID}">
                            <img src="images/remove-icon.png" class="remove-icon">
                            Remove
                        </button>
                    </div>
                    <p class="plot">${Plot}</p>
                </div>
        </section>`
}