let matchedMoviesData
const apiKey = 'cc146581'
const searchForm = document.getElementsByClassName('search-form')[0]
const searchInput = document.getElementsByClassName('search-input')[0]
const watchlistAddBtn = document.getElementsByClassName('watchlist-add-btn')[0]

searchForm.addEventListener('submit', search)
searchInput.addEventListener('focus', setInputValidity)
searchInput.addEventListener('input', setInputValidity)
document.addEventListener('click', (e) => {
    if (e.target.dataset.imdbid) {addToWatchlist(e.target.dataset.imdbid)}
})


function addToWatchlist(imdbid) {
    let watchlist = JSON.parse(localStorage.getItem('watchlist')) || []
    if (!watchlist.includes(imdbid)) {
        watchlist.push(imdbid)
        localStorage.setItem('watchlist', JSON.stringify(watchlist))
    }
    console.log(localStorage.getItem('watchlist'))
}

function setInputValidity() {
    if(searchInput.validity.valueMissing) {
        searchInput.setCustomValidity('Please enter movie name')
    }
    else {
        searchInput.setCustomValidity('')
    }
}

function search(event) {
    event.preventDefault()

    const isValid = searchForm.checkValidity
    if(isValid) {
        const searchInputValue = searchInput.value
        renderMovies(searchInputValue)
    }
    else {
        searchForm.reportValidity()
    }
}

function renderMovies(searchInputValue) {
    // json of matched movies with little details
    fetch(`https://www.omdbapi.com/?apikey=${apiKey}&s=${searchInputValue}&plot=short`)
    .then(response => response.json())
    .then(async data => {
        matchedMoviesData = data
        console.log(data)
        // fetching more detailed data.
        Promise.all(matchedMoviesData.Search.map(movie => fetch(`https://www.omdbapi.com/?apikey=${apiKey}&i=${movie.imdbID}`)))
            .then(responses => Promise.all(responses.map(results => results.json())))
                .then(results => {
                    let moviesHtml = results.map(movie => getMovieHtml(movie))
                    document.querySelector('main').innerHTML = moviesHtml.join('')
                })
        }
    )
}

function getMovieHtml(movieData) {
    const { Genre, imdbRating, Poster, Plot, Runtime, Title, imdbID  } = movieData
    return `
        <section class="movie">
                <img src="${Poster}" class="poster">
                <div class="about-movie">
                    <div class="movie-title">${Title}  
                        <span class="stars">⭐${imdbRating}</span>
                    </div>
                    <div class="movie-info">
                        <span>${Runtime} min</span>
                        <span>${Genre}</span>
                        <button class='watchlist-add-btn' data-imdbID="${imdbID}">
                            <img src="images/plus-icon.png" class="watchlist-icon">
                            Watchlist
                        </button>
                    </div>
                    <p class="plot">${Plot}</p>
                </div>
        </section>`
}


