## Movie search service

## Description
User can search movies and save them to watchlist.
Search is executed with use of omdbapi api service. Watchlist is stored in localStorage.
Service made with using responsive design.
